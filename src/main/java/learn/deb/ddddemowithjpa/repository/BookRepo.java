package learn.deb.ddddemowithjpa.repository;

import learn.deb.ddddemowithjpa.aggregates.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by 114222 on 8/4/2017.
 */
public interface BookRepo extends CrudRepository<Book,Long> {

    @Query("select book from Book book where book.metadata.bookName = :bookName")
    Book findByBookName(@Param("bookName") String bookName);

}
