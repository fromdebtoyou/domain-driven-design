package learn.deb.ddddemowithjpa.repository;

import learn.deb.ddddemowithjpa.aggregates.Book;
import learn.deb.ddddemowithjpa.aggregates.BookCopy;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by 114222 on 8/4/2017.
 */

public interface BookCopyRepo extends CrudRepository<BookCopy,Long> {

}
