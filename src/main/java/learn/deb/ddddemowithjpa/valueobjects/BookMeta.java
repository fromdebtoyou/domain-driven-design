package learn.deb.ddddemowithjpa.valueobjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;

/**
 * Created by 114222 on 8/4/2017.
 */
@Embeddable
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class BookMeta {

    private String bookName;
    private String bookAuthor;
    private String bookGenre;
    private String bookVolume;


}
