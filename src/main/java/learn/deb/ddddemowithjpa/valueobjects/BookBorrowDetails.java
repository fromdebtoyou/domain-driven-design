package learn.deb.ddddemowithjpa.valueobjects;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 114222 on 8/1/2017.
 */
@NoArgsConstructor
@Setter
@Getter
@Embeddable
public class BookBorrowDetails {

    @Column
    private String userId;
    @Column
    private Date borrowedOn;
    @Column
    private int renewedTimes = 0;

}
