package learn.deb.ddddemowithjpa.valueobjects;

import com.sun.istack.internal.Nullable;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.validation.constraints.Null;

/**
 * Created by 114222 on 8/1/2017.
 */
@NoArgsConstructor
@Setter
@Getter
@Embeddable
public class BookShelfSlots {

    private String shelfNo;
    private String rackNo;
    private String position;

}
