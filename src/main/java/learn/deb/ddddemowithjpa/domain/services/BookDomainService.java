package learn.deb.ddddemowithjpa.domain.services;

import learn.deb.ddddemowithjpa.aggregates.Book;
import learn.deb.ddddemowithjpa.aggregates.BookCopy;
import learn.deb.ddddemowithjpa.repository.BookCopyRepo;
import learn.deb.ddddemowithjpa.repository.BookRepo;
import learn.deb.ddddemowithjpa.valueobjects.BookMeta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by 114222 on 8/4/2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class BookDomainService {

    @Autowired
    private BookRepo iBook;

    @Autowired
    private BookCopyRepo iBookCopy;

    public void createABook(BookMeta abook){
        new Book().commissionABook(abook,iBook);
    }

    public void receiveABookInLibrary(String bookName,float price, String yearOfPublication,String edition,String language){
        BookCopy receivedCopy = new BookCopy();
        receivedCopy.receiveTheBookCopy(price, yearOfPublication, edition, language,iBookCopy);

        Book theBook = iBook.findByBookName(bookName);

        receivedCopy.associateWithBook(theBook);

    }

    public void receiveABookInLibrary(BookMeta bkm,float price, String yearOfPublication,String edition,String language){

        Book theBook = iBook.findByBookName(bkm.getBookName());

        if(theBook == null){
            createABook(bkm);
            theBook = iBook.findByBookName(bkm.getBookName());
        }

        BookCopy receivedCopy = new BookCopy();
        receivedCopy.receiveTheBookCopy(price, yearOfPublication, edition, language,iBookCopy);

        receivedCopy.associateWithBook(theBook);

    }


}
