package learn.deb.ddddemowithjpa;

import learn.deb.ddddemowithjpa.aggregates.Book;
import learn.deb.ddddemowithjpa.aggregates.BookCopy;
//import learn.deb.ddddemowithjpa.repository.BookCopyRepo;
import learn.deb.ddddemowithjpa.domain.services.BookDomainService;
import learn.deb.ddddemowithjpa.repository.BookRepo;
import learn.deb.ddddemowithjpa.valueobjects.BookMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DddDemoWithJpaApplication implements CommandLineRunner {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	DataSource dataSource;

	@Autowired
	private BookDomainService bookDomainService;

	@Bean
	public BookDomainService bookDomainService(){
		return new BookDomainService();
	}

	public static void main(String[] args) {
		SpringApplication.run(DddDemoWithJpaApplication.class, args);

	}

	@Transactional(readOnly = false)
	@Override
	public void run(String... args) throws Exception {

		//bookDomainService.createABook(new BookMeta("Lord of the Rings","JRR Tolkien","Fantasy","0.0"));

		//bookDomainService.receiveABookInLibrary(new BookMeta("Two Towers","JRR Tolkien","Fantasy","0.0"),136.95f,"1987","14th","English");
		bookDomainService.receiveABookInLibrary(new BookMeta("Lord of the Rings","JRR Tolkien","Fantasy","0.0"),138.95f,"1989","15th","English");


		System.out.println("Done!");


	}

}
