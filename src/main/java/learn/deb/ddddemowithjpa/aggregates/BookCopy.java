package learn.deb.ddddemowithjpa.aggregates;

import learn.deb.ddddemowithjpa.repository.BookCopyRepo;
import learn.deb.ddddemowithjpa.valueobjects.BookBorrowDetails;
import learn.deb.ddddemowithjpa.valueobjects.BookShelfSlots;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.List;

/**
 * Created by 114222 on 8/1/2017.
 * BookCopy has a collection of BookBorrowDetails. The borrow details can not stand alone and they should be searchable
 * using the book copy only. hence, they do not need an Id. Therefore, they are just Value Objects and not aggregates.
 * Hence, we can use, @Embedded for Value Objects
 *
 */
@Entity
@Setter
@NoArgsConstructor
public class BookCopy {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long bookCopyId;

    @Column
    private float price;
    @Column
    private String yearOfPublication;
    @Column
    private String edition;
    @Column
    private String language;

    @Column
    private Long bookId;

    @Getter
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="shelfNo", column=@Column(nullable=true)),
            @AttributeOverride(name="rackNo", column=@Column(nullable=true)),
            @AttributeOverride(name="position", column=@Column(nullable=true))
    })
    private BookShelfSlots shelfSlot;

    @Getter
    @ElementCollection(targetClass = BookBorrowDetails.class)
    @JoinTable(name = "bookborrowdetails")
    @JoinColumn(name = "bookCopyId", referencedColumnName = "bookCopyId")
    private List<BookBorrowDetails> borrowHistory;


    public void receiveTheBookCopy(float price, String yearOfPublication,String edition,String language,BookCopyRepo iBookCopyRepo){
        this.price = price;
        this.yearOfPublication = yearOfPublication;
        this.edition = edition;
        this.language = language;

        iBookCopyRepo.save(this);

    }

    public void purgeTheBookCopy(){

    }

    public void placeTheBookCopyOnShelf(BookShelfSlots location){
        this.shelfSlot.setShelfNo(location.getShelfNo());
        this.shelfSlot.setRackNo(location.getRackNo());
        this.shelfSlot.setPosition(location.getPosition());

    }

    public void fetchTheBookCopyFromShelf(){
        this.shelfSlot = null;
    }

   /* public void editBookCopyDetails(String yearOfPublication,String edition,String language, float price){
        receiveTheBookCopy(price,yearOfPublication,edition,language);
    }*/

    public void lendTheCopy(String bookCopyId){

    }

    public void renewTheCopy(String bookCopyId){

    }

    public void associateWithBook(Book book){
        this.bookId = book.getBookId();
    }

    public boolean isAvailable(){
        return true;
    }
}
