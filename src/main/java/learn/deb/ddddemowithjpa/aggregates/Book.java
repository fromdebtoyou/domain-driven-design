package learn.deb.ddddemowithjpa.aggregates;

import learn.deb.ddddemowithjpa.repository.BookCopyRepo;
import learn.deb.ddddemowithjpa.repository.BookRepo;
import learn.deb.ddddemowithjpa.valueobjects.BookMeta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.persistence.*;
import java.util.List;

/**
 * Created by 114222 on 8/1/2017.
 * Metadata is for book. So it is an embeddable object for Book. They do not need an Id or a table (!!!)
 */
@Entity
@Setter
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long bookId;

    /****************Book Metadata *****************/
    @Getter
    @Embedded
    private BookMeta metadata;
    /****************Book Metadata *****************/

    @Transient
    @Getter
    private List<BookCopy> copiesOfBook;

    public void commissionABook(BookMeta bmk,BookRepo iBook){

        this.metadata = new BookMeta();

        this.metadata.setBookName(bmk.getBookName());
        this.metadata.setBookAuthor(bmk.getBookAuthor());
        this.metadata.setBookGenre(bmk.getBookGenre());
        this.metadata.setBookVolume(bmk.getBookVolume());

        iBook.save(this);
    }


    public void procureBookCopies(int bookCopies){


    }

    public void decommisionABook(String name){

    }

    public Long checkCopiesAvailable(){
        return this.copiesOfBook
                    .stream()
                    .filter(bookCopy -> bookCopy.isAvailable()==true)
                    .count();
    }


}
